

var drawDays= function(days,schedule,agenda){
    var dayCol = "";
    //var agenda = populateAgenda(schedule);
    
    days.forEach(showDays);
    
    
};

  var schedule = {
    "monday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "tuesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "11:30", "end_time": "12:30"},
        {"name": "Felipe", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "wednesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Francisco", "start_time": "10:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "thursday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "friday": [
        {"name": "Francisco", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Francisco", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Andres", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Francisco", "start_time": "17:00", "end_time": "19:30"}
    ]
};
var populateRanges = function(){
    
    var ranges = [];
    for( var i = 6; i<24; i++){
        var currentHour = i + ":" + "00";
        if(i <10){
            currentHour = "0"+currentHour;
        }
        ranges.push(currentHour);
        var midHour = i + ":" + "30";
        if(i<10){
            midHour = "0"+midHour;
        }
        ranges.push(midHour);
    }
    return ranges;
}
var populateAgenda = function(schedule){
    var ranges = populateRanges();
    var agendMap = new Object;
    jQuery.each(schedule, function(day,schedArray){
        
        schedArray.forEach(function(schedObject){
        
        var selectedRanges = [];
        var startRange = -1;
        var endRange   = -1; 
        var rangeFound = false;    
            for(var k=0; k<=ranges.length; k++){
                if(schedObject.start_time.localeCompare(ranges[k])==0){
                    startRange = k;
                    rangeFound = true;
                    
                }
                if(schedObject.end_time.localeCompare(ranges[k])==0){
                    endRange = k;
                    break;
                }
                
            }
        if(rangeFound){
           
            for(var j=startRange; j<=endRange;j++){
                
                var thisTime = ranges[j];
                var selector = 'div.cell-hour-'+thisTime;
                $(""+selector+"").html(schedObject.name);
                agendMap["day-"+day+"-hour-"+thisTime]= schedObject.name;
            }
            
        }   
        });
        
        
    });
    
    return agendMap;
}
var agenda = populateAgenda(schedule);


var showDays = function(value,index){
      
    var ranges = populateRanges();
    var dayCol = '<div class="col-sm-1 day-'+value+'">'+value+ initCells(value,ranges)+'</div>';
    $(".agenda").append(dayCol);
    
}


var initTimeColumn = function(){
    
    var ranges = populateRanges();
    var dayCol = '<div class="col-sm-1 day-none">'+ initCells(undefined,ranges)+'</div>';
    $(".day-monday").parent().prepend(dayCol);
    $(".day-sunday").parent().append(dayCol);
    
}

var initCells = function(day,ranges){
    
    var rows = '';
    if(day == undefined){
        rows+='<div class="row"><div class="col-sm-1"></div></div>"';    
    }
    
    ranges.forEach(function(k){
        var cellClass = 'cell-day-'+'none'+ ' cell-hour-'+k;
        if(day == undefined){
            cellClass = 'cell-day-'+day+ ' cell-hour-'+k;    
            rows+='<div class="row"><div class="col-sm-1 '+ cellClass+'">'+k+ "</div></div>";
        }else{
            
            var mapKey = "day-"+day+"-hour-"+k;
            var people = "&nbsp;";
            if(agenda[mapKey] != undefined){
                people = agenda[mapKey];
            }
            
            cellClass = 'cell-day-'+day+ ' cell-hour-'+k;    
            rows+='<div class="row"><div class="col-sm-1 '+ cellClass+'">'+people+ "</div></div>";
        }
    });
    return rows;
}






$( document ).ready(function() {
    
    
        var days = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"];
        
        drawDays(days,schedule);
        initTimeColumn();
      });