# Test

Un colegio necesita un sistema para administrar sus cursos. El
sistema tiene que soportar que se ingresen varios cursos. Cada curso
tendrá un profesor a cargo y una serie de alumnos inscritos. Cada
profesor, así como cada alumno puede estar en más de un curso. Además
cada curso tendrá una cantidad no determinada de pruebas, y el sistema
debe permitir ingresar la nota para cada alumno en cada prueba. Todas las
pruebas valen lo mismo.

## Modelo de datos

Escriba a continuación las tablas que utilizaría para resolver este
problema con los campos y llaves de éstas. Intente hacer el sistema lo
más robusto posible, pero sin incluir datos adicionales a los que se
plantean acá.

### Respuesta
```sql
CREATE TABLE curso(
    id serial UNIQUE,
    nombre varchar(255) NOT NULL UNIQUE PRIMARY KEY
    
);


CREATE TABLE profesor(
    id serial UNIQUE PRIMARY KEY,
    nombre text ,
    apellido text 
    
);

CREATE TABLE alumno(
    id serial UNIQUE PRIMARY KEY,
    nombre text NOT NULL,
    apellido text NOT NULL
    
);



CREATE TABLE cursada(

    id serial UNIQUE PRIMARY KEY,
    id_profesor integer references profesor(id),
    id_curso    integer references curso(id),
    CONSTRAINT cursada_pk UNIQUE(id_profesor,id_curso)
    
);

CREATE TABLE inscripcion(
    id serial UNIQUE PRIMARY KEY,
    id_alumno integer references alumno(id),
    id_cursada  integer references cursada(id),
    CONSTRAINT inscripcion_pk UNIQUE(id_alumno,id_cursada)

);

CREATE TABLE prueba(
    id serial UNIQUE PRIMARY KEY,
    id_cursada integer references cursada(id),
    nombre text NOT NULL, 
    CONSTRAINT prueba_pk UNIQUE(nombre,id_cursada)
);

CREATE TABLE nota(
    id serial UNIQUE PRIMARY KEY,
    id_alumno integer references alumno(id),
    id_prueba integer references prueba(id),
    nota float,
    CONSTRAINT nota_pk UNIQUE(id_alumno,id_prueba)
);


insert into alumno(nombre,apellido) values ('francisco','valenzuela');
insert into alumno(nombre,apellido) values ('paula','quezada');
insert into alumno(nombre,apellido) values ('andres','gomez');

insert into profesor(nombre,apellido) values('carlos','undurraga');
insert into profesor(nombre,apellido) values('juan','santis');
insert into profesor(nombre,apellido) values('patricio','poblete');

insert into curso(nombre) values('matematicas');
insert into curso(nombre) values('historia');
insert into curso(nombre) values('programacion');

insert into cursada(id_profesor,id_curso) values(1,1);
insert into cursada(id_profesor,id_curso) values(2,2);
insert into cursada(id_profesor,id_curso) values(3,3);


insert into inscripcion(id_alumno,id_cursada) values(1,1);
insert into inscripcion(id_alumno,id_cursada) values(1,2);
insert into inscripcion(id_alumno,id_cursada) values(1,3);
insert into inscripcion(id_alumno,id_cursada) values(2,1);
insert into inscripcion(id_alumno,id_cursada) values(3,1);
insert into inscripcion(id_alumno,id_cursada) values(2,2);


insert into prueba(nombre,id_cursada) values('p1',1);
insert into prueba(nombre,id_cursada) values('p2',1);
insert into prueba(nombre,id_cursada) values('p2',2);


insert into nota(id_alumno,id_prueba,nota) values(1,1,6.0);
insert into nota(id_alumno,id_prueba,nota) values(1,2,7.0);
insert into nota(id_alumno,id_prueba,nota) values(1,3,3.0);


```


## SQL

Considerando el enunciado anterior conteste las siguientes preguntas:

1. Escriba una Query que entregue la lista de  alumnos para el curso
"programación"
### Respuesta
```sql
SELECT alumno.id, alumno.nombre, alumno.apellido FROM curso 
inner join cursada  ON cursada.id_curso = curso.id 
inner join inscripcion on inscripcion.id_cursada = cursada.id
inner join alumno on alumno.id = inscripcion.id_alumno 
where curso.nombre = 'programacion';

```

2. Escriba una Query  que calcule el promedio de notas de un alumno en un
curso.

### Respuesta
```sql

    select id_alumno, avg(nota) as promedio from nota where id_alumno = '1' group by id_alumno;
```


3. Escriba una Query que entregue a los alumnos y el promedio que tiene
en cada curso.
### Respuesta
```sql

select id_alumno, avg(nota),curso.nombre from nota 
inner join prueba on prueba.id = nota.id_prueba
inner join cursada on cursada.id = prueba.id_cursada
inner join curso on curso.id = cursada.id_curso
group by id_alumno,curso.nombre;
```
4. Escriba una Query que lista a todos los alumnos con más de un curso con

promedio rojo.
### Respuesta
```sql
```
5. Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis:
`PLAYERS(Nombre, Pais, Ranking)`. Suponga que Ranking es un número de 1 a
100 que es distinto para cada jugador. Si la tabla en un momento dado
tiene
solo 20 registros, indique cuantos registros tiene la tabla que resulta de la
siguiente consulta:

```
SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking
```
Seleccione las respuestas correctas:

```
a) 400
b) 190
c) 20
d) imposible saberlo
```
# Respuesta
20 ya que si todos los números son distintos, el query va a devolver los
jugadores ordenados por ranking


## Diseño de software

### Backend

Si usted estuviera resolviendo el problema del colegio implementando una aplicación web que incluya las siguientes funcionalidades:

1. CRUD alumnos, cursos, pruebas y notas.
2. Listar a los alumnos junto a su promedio de notas.
3. Filtar a todos los alumnos con más de un ramo con promedio rojo.

Para la implementación hay que utilizar el [Django](https://www.djangoproject.com/) y su ORM.

**Nota:** La aplicación debe incluir un archivo README.md explicando como instalar las dependencias del proyecto y todos los supuestos considerados.

### Frontend

Construya una función o clase en JS que recibiendo el siguiente JSON por
parámetro, permita renderear una agenda semanal en html y con bloques de
30 minutos como la siguiente:

```
{
    "monday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "11:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "tuesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "11:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "wednesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "10:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "thursday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "friday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ]
}
```

![Calendar](https://www.dropbox.com/s/1x3uq95cqysxp8t/calendar.png?raw=1)

La agenda debe contener los distintos bloques y pintar con el nombre del
paciente, las horas que están tomadas.

**Nota:**

* La agenda NO debe tener interacción solo dibujarse en la pantalla
* No utilizar tablas, sólo DIVS
* La agenda debe tener un ancho de 960px y esta centrada en la pantalla

# Respuesta
Ver archivos incluidos dentro de directorio front end